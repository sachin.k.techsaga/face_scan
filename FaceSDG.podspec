
Pod::Spec.new do |spec|

  spec.name         = "FaceSDG"
  spec.version      = "1.0.0"
  spec.summary      = "FaceScan"
  spec.description  = "This is the app description."
  spec.homepage     = "http://EXAMPLE/FaceScan"
  spec.license      = "MIT"
  spec.author             = { "Sachin" => "sachin.k.techsaga@gmail.com" }
  spec.platform     = :ios, "13.0"
  spec.source       = { :git => "https://gitlab.com/sachin.k.techsaga/face_scan.git", :tag => "1.0.2"}
  spec.source_files  = "FaceSDG"
  spec.exclude_files = "Classes/Exclude"
  spec.swift_version = "5.0"

  spec.dependency 'GoogleMLKit/FaceDetection', '3.2.0'

end
