//
//  ResultVC.swift
//  VisionExample
//
//  Created by Techsaga Corp on 06/12/22.
//  Copyright © 2022 Google Inc. All rights reserved.
//

import UIKit

class ResultVC: UIViewController, UITextViewDelegate {
    
    @IBOutlet weak var textLbl: UITextView!
    var str = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        self.textLbl.delegate = self
        textLbl.text! = str
        DispatchQueue.main.async {
            self.textLbl.text = UserDefaults.standard.string(forKey: "jsonData")//data
            self.textLbl.isScrollEnabled = true
            self.textLbl.textColor = UIColor.black
            self.textLbl.isEditable = true
            self.textLbl.becomeFirstResponder() //puts cursor on text field
            self.textLbl.selectAll(nil)  //highlights text
            self.textLbl.selectAll(self)
        }
    }
}
