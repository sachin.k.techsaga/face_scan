//
//  CameraViewController.swift
//  VisionExample
//
//  Created by Techsaga Corp on 14/12/22.
//  Copyright © 2022 Google Inc. All rights reserved.
//

import AVFoundation
import CoreVideo
import MLImage
//import MLKit
import MLKitVision
import MLKitFaceDetection
import MLKitCommon
import CallKit

@objc(CameraViewController)
public class CameraViewController: UIViewController, CXCallObserverDelegate {
    
    private let detectors: [Detector] = [
        .onDeviceFace
    ]
    @IBOutlet weak var bottumView: UIView!
    
    private var currentDetector: Detector = .onDeviceFace
    private var isUsingFrontCamera = true
    private var previewLayer: AVCaptureVideoPreviewLayer!
    private lazy var captureSession = AVCaptureSession()
    private lazy var sessionQueue = DispatchQueue(label: Constant.sessionQueueLabel)
    private var lastFrame: CMSampleBuffer?
    
    @IBOutlet weak var proceedLbl: UILabel!
    @IBOutlet weak var persentageLbl: UILabel!
    @IBOutlet weak var alertLbl: UILabel!
    @IBOutlet weak var alertView: UIView!
    
    @IBOutlet weak var cancelBtn: UIButton!
    
    
    public var delegate: CallbackListenrs?
    
//    public lazy var cgPointEyeLeft = CGPoint()
//    public lazy var cgPointEyeRight = CGPoint()
//
//    public lazy var cgPointCheekLeft = CGPoint()
//    public lazy var cgPointCheekRight = CGPoint()
    
    var mainImageBuffer: CVImageBuffer?
    
    let pixelThinner = 5
    var equipments = [[String:Double]]()
//    var startTime:Int = 10
    var jsonString:String?
    var fps:Int = 0
    var ppg_TimeArr:[Int] = []
    var StartTimeStamp: Int?
    var average_fps:Int?
    var countB = 0
    var countA = 0
    var time_t = 0;
    var lastUpdateTime: TimeInterval = 0
    
    var percentageCompleted = Float()
    
    var isFaceDetected = Bool()
    var type = ""
    
    var timer : Timer?
    var counter = 0
    
    var fpsCount = 0
    
    var leftR = Double()
    var leftG = Double()
    var leftB = Double()
    var rightR = Double()
    var rightG = Double()
    var rightB = Double()
    
    var faceId = 0
    
    var cancel_timer : Timer?
    var cancel_counter = 0
    var firstTime = 1
    
    var cancel_fram_timer : Timer?
    var cancel_fram_counter = 0
    var first_fram_Time = 1
    
    var mfps = 0
    var mfpsCounter = 0
    var mfpsTime = 0
    
    var calibration_timer : Timer?
    var calibrationTime = 20
    var maxTime = 0
    var minTime = 0
    var TextColor = ""
    var BoxColor = ""
    var BoxType = ""
    
    var callObserver = CXCallObserver()
        
    private lazy var previewOverlayView: UIImageView = {
        
        precondition(isViewLoaded)
        let previewOverlayView = UIImageView(frame: .zero)
        previewOverlayView.contentMode = UIView.ContentMode.scaleAspectFill
        previewOverlayView.translatesAutoresizingMaskIntoConstraints = false
        return previewOverlayView
    }()
    
    private lazy var annotationOverlayView: UIView = {
        precondition(isViewLoaded)
        let annotationOverlayView = UIView(frame: .zero)
        annotationOverlayView.translatesAutoresizingMaskIntoConstraints = false
        return annotationOverlayView
    }()
    
    private var lastDetector: Detector?
    
    // MARK: - IBOutlets
    
    @IBOutlet private weak var cameraView: UIView!
    
    // MARK: - UIViewController
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        
        callObserver.setDelegate(self, queue: nil)
        
        self.cancelBtn.clipsToBounds = true
        self.cancelBtn.layer.cornerRadius = 20
        
        UIApplication.shared.isIdleTimerDisabled = true
        
        self.bottumView.isHidden = true
        
        self.proceedLbl.textColor = UIColor(hexString: self.TextColor)
//        print("viewDidLoad:===== \(self.TextColor)")
        
        if AVCaptureDevice.authorizationStatus(for: AVMediaType.video) ==  AVAuthorizationStatus.authorized {
            // Already Authorized
            print("Already Authorized")
            DispatchQueue.main.async {
                self.bottumView.isHidden = false
                self.previewLayer = AVCaptureVideoPreviewLayer(session: self.captureSession)
                self.previewLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
                self.setUpPreviewOverlayView()
                self.setUpAnnotationOverlayView()
                self.setUpCaptureSessionOutput()
                self.setUpCaptureSessionInput()
                self.counter = 0
            }
        } else {
            AVCaptureDevice.requestAccess(for: AVMediaType.video, completionHandler: { (granted: Bool) -> Void in
               if granted == true {
                   // User granted
                   print("User granted")
                   DispatchQueue.main.async {
                       self.bottumView.isHidden = false
                       self.previewLayer = AVCaptureVideoPreviewLayer(session: self.captureSession)
                       self.previewLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
                       self.setUpPreviewOverlayView()
                       self.setUpAnnotationOverlayView()
                       self.setUpCaptureSessionOutput()
                       self.setUpCaptureSessionInput()
                       self.counter = 0
                   }
               } else {
                   // User rejected
                   print("User rejected")
                   DispatchQueue.main.async {
                       let alertController = UIAlertController(title: "Alert", message: "Please allow camera permission" , preferredStyle: .alert)
                       alertController.addAction(UIAlertAction (title: "Ok", style: .default, handler: { (action:UIAlertAction) in
                           UIApplication.shared.isIdleTimerDisabled = false
                           self.dismiss(animated: true)
                       }))
                       self.present(alertController, animated: true, completion: nil)
                   }
               }
           })
        }
        
    }
    
    public override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return UIInterfaceOrientationMask.portrait
    }

    public override var shouldAutorotate: Bool {
        return false
    }
    
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        UIApplication.shared.isIdleTimerDisabled = true
        self.proceedLbl.textColor = UIColor(hexString: self.TextColor)
//        print("viewWillAppear:===== \(self.TextColor)")
        if AVCaptureDevice.authorizationStatus(for: AVMediaType.video) ==  AVAuthorizationStatus.authorized {
            // Already Authorized
            print("Already Authorized")
            DispatchQueue.main.async {
                self.calibration_timer = Timer.scheduledTimer(timeInterval:1, target:self, selector:#selector(self.calibrationTimer), userInfo: nil, repeats: true)
                self.equipments.removeAll()
                self.ppg_TimeArr.removeAll()
                self.time_t = 0
                self.average_fps = 0
                self.fpsCount = 0
                self.calibrationTime = 20
                self.StartTimeStamp = Int(NSDate().timeIntervalSince1970 * 1000)
                self.proceedLbl.text! = "Scan Starts in 20"
                self.startSession()
            }
        } else {
            AVCaptureDevice.requestAccess(for: AVMediaType.video, completionHandler: { (granted: Bool) -> Void in
               if granted == true {
                   // User granted
                   print("User granted")
                   DispatchQueue.main.async {
                       self.calibration_timer = Timer.scheduledTimer(timeInterval:1, target:self, selector:#selector(self.calibrationTimer), userInfo: nil, repeats: true)
                       self.equipments.removeAll()
                       self.ppg_TimeArr.removeAll()
                       self.time_t = 0
                       self.average_fps = 0
                       self.fpsCount = 0
                       self.calibrationTime = 20
                       self.StartTimeStamp = Int(NSDate().timeIntervalSince1970 * 1000)
                       self.proceedLbl.text! = "Scan Starts in 20"
                       self.startSession()
                   }
               } else {
                   // User rejected
                   print("User rejected")
                   DispatchQueue.main.async {
                       let alertController = UIAlertController(title: "Alert", message: "Please allow camera permission" , preferredStyle: .alert)
                       alertController.addAction(UIAlertAction (title: "Ok", style: .default, handler: { (action:UIAlertAction) in
                           UIApplication.shared.isIdleTimerDisabled = false
                           self.dismiss(animated: true)
                       }))
                       self.present(alertController, animated: true, completion: nil)
                   }
               }
           })
        }
        
        self.equipments.removeAll()
        self.ppg_TimeArr.removeAll()
        self.time_t = 0
        self.average_fps = 0
        self.fpsCount = 0
        self.calibrationTime = 20
        self.StartTimeStamp = Int(NSDate().timeIntervalSince1970 * 1000)
        self.proceedLbl.text! = "Scan Starts in 20"
        
    }
    
    public func callObserver(_ callObserver: CXCallObserver, callChanged call: CXCall) {
        
        if call.isOutgoing == true && call.hasConnected == false && call.hasEnded == false {
            //.. 1. detect a dialing outgoing call
        }
        if call.isOutgoing == true && call.hasConnected == true && call.hasEnded == false {
            //.. 2. outgoing call in process
        }
        if call.isOutgoing == false && call.hasConnected == false && call.hasEnded == false {
            //.. 3. incoming call ringing (not answered)
        }
        if call.isOutgoing == false && call.hasConnected == true && call.hasEnded == false {
            //.. 4. incoming call in process
        }
        if call.isOutgoing == true && call.hasEnded == true {
            //.. 5. outgoing call ended.
        }
        if call.isOutgoing == false && call.hasEnded == true {
            //.. 6. incoming call ended.
        }
        if call.hasConnected == true && call.hasEnded == false && call.isOnHold == false {
            //.. 7. call connected (either outgoing or incoming)
            print("incoming call")
            stopSession(message: "Face scan Error - app is in background")
            self.timer?.invalidate()
            self.timer = nil
            self.counter = 0
            self.calibrationTime = 20
            UIApplication.shared.isIdleTimerDisabled = false
//            delegate?.onCancelScan(cancelError: "Face scan Error - app is in background")
            self.dismiss(animated: true)
        }
        if call.isOutgoing == true && call.isOnHold == true {
            //.. 8. outgoing call is on hold
        }
        if call.isOutgoing == false && call.isOnHold == true {
            //.. 9. incoming call is on hold
        }
    }
    
    public override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        if AVCaptureDevice.authorizationStatus(for: AVMediaType.video) ==  AVAuthorizationStatus.authorized {
            // Already Authorized
            print("Already Authorized")
            DispatchQueue.main.async {
                self.stopSession(message: "")
            }
        } else {
            AVCaptureDevice.requestAccess(for: AVMediaType.video, completionHandler: { (granted: Bool) -> Void in
               if granted == true {
                   // User granted
                   print("User granted")
                   DispatchQueue.main.async {
                       self.stopSession(message: "")
                   }
               } else {
                   // User rejected
//                   print("User rejected")
                   DispatchQueue.main.async {
                       let alertController = UIAlertController(title: "Alert", message: "Please allow camera permission" , preferredStyle: .alert)
                       alertController.addAction(UIAlertAction (title: "Ok", style: .default, handler: { (action:UIAlertAction) in
                           UIApplication.shared.isIdleTimerDisabled = false
                           self.dismiss(animated: true)
                       }))
                       self.present(alertController, animated: true, completion: nil)
                   }
               }
           })
        }
        
    }
    
    public override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        
        if AVCaptureDevice.authorizationStatus(for: AVMediaType.video) ==  AVAuthorizationStatus.authorized {
            // Already Authorized
            print("Already Authorized")
            DispatchQueue.main.async {
                self.previewLayer.frame = self.cameraView.frame
            }
        } else {
            AVCaptureDevice.requestAccess(for: AVMediaType.video, completionHandler: { (granted: Bool) -> Void in
               if granted == true {
                   // User granted
                   print("User granted")
                   DispatchQueue.main.async {
                       self.previewLayer.frame = self.cameraView.frame
                   }
               } else {
                   // User rejected
                   print("User rejected")
               }
           })
        }
    }
    
    // MARK: - IBActions
    func currentTimeInMilliSeconds()-> Int
    {
        let currentDate = Date()
        let since1970 = currentDate.timeIntervalSince1970
        return Int(since1970 * 1000)
    }
    
    @IBAction func cancelScanAction(_ sender: UIButton) {
//        delegate?.onCancelScan(cancelError: "Scan Canceled")
        self.calibration_timer?.invalidate()
        self.calibration_timer = nil
        
        self.timer?.invalidate()
        self.timer = nil
        self.counter = 0
        self.calibrationTime = 20
        
        self.cancel_timer?.invalidate()
        self.cancel_timer = nil
        self.cancel_counter = 0
        
        self.cancel_fram_timer?.invalidate()
        self.cancel_fram_timer = nil
        self.cancel_fram_counter = 0
        
        self.timer?.invalidate()
        self.timer = nil
        self.counter = 0
        self.calibrationTime = 20
        self.delegate?.onScanFinished(raw_intensity: "", ppg_time: "", average_fps: self.fpsCount, finalData: "")
        stopSession(message: "Scan Canceled")
        UIApplication.shared.isIdleTimerDisabled = false
        self.dismiss(animated: true)
    }
    
    @objc func calibrationTimer() {
        self.calibrationTime -= 1
        if self.calibrationTime < 1 {
            self.calibration_timer?.invalidate()
            self.calibration_timer = nil
//            self.calibrationTime = 20
            self.timer = Timer.scheduledTimer(timeInterval:1, target:self, selector:#selector(self.prozessTimer), userInfo: nil, repeats: true)
        }
        else {
            self.persentageLbl.text! = "Calibration in Progress..."
            self.proceedLbl.text! = "Scan Starts in \(self.calibrationTime)"
            self.type = "Calibration"
        }
    }
    
    @objc func prozessTimer() {
        self.proceedLbl.textColor = UIColor(hexString: self.TextColor)
        print("prozessTimer:===== \(self.TextColor)")
        DispatchQueue.main.async{
            //            if UIDevice.current.orientation == UIDeviceOrientation.landscapeLeft || UIDevice.current.orientation == UIDeviceOrientation.landscapeRight || UIDevice.current.orientation == UIDeviceOrientation.portraitUpsideDown {
            //                self.stopSession()
            //                self.delegate?.onCancelScan(cancelError: "Unable to measure your vitals.\nTry to look at the camera the next time")
            //                self.dismiss(animated: true)
            //            }
            if UIApplication.shared.applicationState == .background {
                // Add code here...
                print("Face scan Error - app is in background")
                //                self.delegate?.onCancelScan(cancelError: "Face scan Error - app is in background")
                self.delegate?.onScanFinished(raw_intensity: "", ppg_time: "", average_fps: self.fpsCount, finalData: "")
                self.stopSession(message: "Face scan Error - app is in background")
                self.timer?.invalidate()
                self.timer = nil
                self.counter = 0
                self.calibrationTime = 20
                UIApplication.shared.isIdleTimerDisabled = false
                self.dismiss(animated: true)
            }
        }
        
        self.counter += 1
        print("counter: ====== \(counter)")
        if self.counter == (self.maxTime-1) {
            print("counter: ====== \(counter)")
            self.persentageLbl.text! = "Face Completed"
            self.presentResult(average_fps: self.average_fps ?? 0)
            
            self.timer?.invalidate()
            self.timer = nil
            self.counter = 0
            self.calibrationTime = 20
        }
        
        self.proceedLbl.text! = "Scan in progress..."
        self.time_t += 1;
        self.percentageCompleted = round((Float(self.time_t)/Float(self.maxTime))*100)
    }
    
    @objc func CancelTimer() {
        self.cancel_counter += 1
        if self.cancel_counter > 3 {
//            delegate?.onCancelScan(cancelError: "Unable to measure your vitals.\nTry to look at the camera the next time")
            self.timer?.invalidate()
            self.timer = nil
            self.counter = 0
            self.calibrationTime = 20
            
            self.cancel_timer?.invalidate()
            self.cancel_timer = nil
            self.cancel_counter = 0
            self.delegate?.onScanFinished(raw_intensity: "", ppg_time: "", average_fps: self.fpsCount, finalData: "")
            self.stopSession(message: "Unable to measure your vitals.\nTry to look at the camera the next time")
            UIApplication.shared.isIdleTimerDisabled = false
            self.dismiss(animated: true)
        }
    }
    
    @objc func CancelFramTimer() {
        self.cancel_fram_counter += 1
        if self.cancel_fram_counter > 3 {
//            delegate?.onCancelScan(cancelError: "Unable to measure your vitals.\nTry to look at the camera the next time")
            self.timer?.invalidate()
            self.timer = nil
            self.counter = 0
            self.calibrationTime = 20
            
            self.cancel_fram_timer?.invalidate()
            self.cancel_fram_timer = nil
            self.cancel_fram_counter = 0
            self.delegate?.onScanFinished(raw_intensity: "", ppg_time: "", average_fps: self.fpsCount, finalData: "")
            self.stopSession(message: "Unable to measure your vitals.\nTry to look at the camera the next time")
            UIApplication.shared.isIdleTimerDisabled = false
            self.dismiss(animated: true)
        }
    }
    
    @IBAction func selectDetector(_ sender: Any) {
        presentDetectorsAlertController()
    }
    
    @IBAction func switchCamera(_ sender: Any) {
        isUsingFrontCamera = !isUsingFrontCamera
        removeDetectionAnnotations()
        setUpCaptureSessionInput()
    }
    
    private func detectFacesOnDevice(in image: VisionImage, width: CGFloat, height: CGFloat) {
        // When performing latency tests to determine ideal detection settings, run the app in 'release'
        // mode to get accurate performance metrics.
        let options = FaceDetectorOptions()
        options.landmarkMode = .all
        options.contourMode = .none
        options.classificationMode = .none
        options.performanceMode = .fast
        options.isTrackingEnabled = true
        let faceDetector = FaceDetector.faceDetector(options: options)
        var faces: [Face] = []
        var detectionError: Error?
        do {
            faces = try faceDetector.results(in: image)
        } catch let error {
            detectionError = error
        }
        weak var weakSelf = self
        DispatchQueue.main.sync {
            guard let strongSelf = weakSelf else {
                print("Self is nil!")
                return
            }
            strongSelf.updatePreviewOverlayViewWithLastFrame()
            if let detectionError = detectionError {
                print("Failed to detect faces with error: \(detectionError.localizedDescription).")
                return
            }
            guard !faces.isEmpty else {
//                print("On-Device face detector returned no results.")
                self.isFaceDetected = false
                if self.calibrationTime > 0 {
                    self.persentageLbl.text! = "Calibration in Progress..."
                    self.alertView.isHidden = false
                    self.alertLbl.text! = "Cannot Detect Face"
                    
                    self.cancel_timer?.invalidate()
                    self.cancel_timer = nil
                    self.cancel_counter = 0
                }
                else {
//                    self.cancel_timer?.invalidate()
//                    self.cancel_timer = nil
//                    self.cancel_counter = 0
                    if self.firstTime == 1 {
                        self.firstTime = 0
                        if self.cancel_fram_counter == 0 {
                            self.cancel_timer = Timer.scheduledTimer(timeInterval: 1, target:self, selector:#selector(CancelTimer), userInfo: nil, repeats: true)
                        }
                    }
                    self.alertView.isHidden = false
                    self.alertLbl.text! = "Cannot Detect Face"
                    self.persentageLbl.text! = "\(lroundf(Float(percentageCompleted)))% Completed"
                }
                if self.counter == self.maxTime {
                    self.persentageLbl.text! = "Face Completed"
                }
                return
            }
            
             if faces != [] {
                 
                let face = faces[0]
                 
                let normalizedRect = CGRect(
                    x: face.frame.origin.x / width,
                    y: face.frame.origin.y / height,
                    width: face.frame.size.width / width,
                    height: face.frame.size.height / height
                )
                let standardizedRect = strongSelf.previewLayer.layerRectConverted(
                    fromMetadataOutputRect: normalizedRect
                ).standardized
                 if self.BoxType == "Box" {
                     UIUtilities.addRectangle(
                        standardizedRect,
                        to: strongSelf.annotationOverlayView,
                        color: UIColor(hexString: self.BoxColor)
                     )
                 }
                 else {
                     let d = round((standardizedRect.width)*0.1)
                     UIUtilities.addLineSegment(fromPoint: CGPoint(x: standardizedRect.minX, y: standardizedRect.minY), toPoint: CGPoint(x: standardizedRect.minX+d, y: standardizedRect.minY), inView: strongSelf.annotationOverlayView, color: UIColor(hexString: self.BoxColor), width: 2)
                     UIUtilities.addLineSegment(fromPoint: CGPoint(x: standardizedRect.minX, y: standardizedRect.minY), toPoint: CGPoint(x: standardizedRect.minX, y: standardizedRect.minY+d), inView: strongSelf.annotationOverlayView, color: UIColor(hexString: self.BoxColor), width: 2)
                     UIUtilities.addLineSegment(fromPoint: CGPoint(x: standardizedRect.maxX, y: standardizedRect.minY), toPoint: CGPoint(x: standardizedRect.maxX-d, y: standardizedRect.minY), inView: strongSelf.annotationOverlayView, color: UIColor(hexString: self.BoxColor), width: 2)
                     UIUtilities.addLineSegment(fromPoint: CGPoint(x: standardizedRect.maxX, y: standardizedRect.minY), toPoint: CGPoint(x: standardizedRect.maxX, y: standardizedRect.minY+d), inView: strongSelf.annotationOverlayView, color: UIColor(hexString: self.BoxColor), width: 2)
                     UIUtilities.addLineSegment(fromPoint: CGPoint(x: standardizedRect.maxX, y: standardizedRect.maxY), toPoint: CGPoint(x: standardizedRect.maxX, y: standardizedRect.maxY-d), inView: strongSelf.annotationOverlayView, color: UIColor(hexString: self.BoxColor), width: 2)
                     UIUtilities.addLineSegment(fromPoint: CGPoint(x: standardizedRect.maxX, y: standardizedRect.maxY), toPoint: CGPoint(x: standardizedRect.maxX-d, y: standardizedRect.maxY), inView: strongSelf.annotationOverlayView, color: UIColor(hexString: self.BoxColor), width: 2)
                     UIUtilities.addLineSegment(fromPoint: CGPoint(x: standardizedRect.minX, y: standardizedRect.maxY), toPoint: CGPoint(x: standardizedRect.minX+d, y: standardizedRect.maxY), inView: strongSelf.annotationOverlayView, color: UIColor(hexString: self.BoxColor), width: 2)
                     UIUtilities.addLineSegment(fromPoint: CGPoint(x: standardizedRect.minX, y: standardizedRect.maxY), toPoint: CGPoint(x: standardizedRect.minX, y: standardizedRect.maxY-d), inView: strongSelf.annotationOverlayView, color: UIColor(hexString: self.BoxColor), width: 2)
                 }
                strongSelf.addContours(for: face, width: width, height: height)
            }
        }
    }
    
    
    // MARK: - Private
    
    private func setUpCaptureSessionOutput() {
        weak var weakSelf = self
        sessionQueue.async {
            guard let strongSelf = weakSelf else {
                print("Self is nil!")
                return
            }
            strongSelf.captureSession.beginConfiguration()
            // When performing latency tests to determine ideal capture settings,
            // run the app in 'release' mode to get accurate performance metrics
            strongSelf.captureSession.sessionPreset = AVCaptureSession.Preset.medium
            
            let output = AVCaptureVideoDataOutput()
            output.videoSettings = [
                (kCVPixelBufferPixelFormatTypeKey as String): kCVPixelFormatType_32BGRA
            ]
            output.alwaysDiscardsLateVideoFrames = true
            let outputQueue = DispatchQueue(label: Constant.videoDataOutputQueueLabel)
            output.setSampleBufferDelegate(strongSelf, queue: outputQueue)
            guard strongSelf.captureSession.canAddOutput(output) else {
                print("Failed to add capture session output.")
                return
            }
            strongSelf.captureSession.addOutput(output)
            strongSelf.captureSession.commitConfiguration()
        }
    }
    
    private func setUpCaptureSessionInput() {
        weak var weakSelf = self
        sessionQueue.async {
            guard let strongSelf = weakSelf else {
                print("Self is nil!")
                return
            }
            let cameraPosition: AVCaptureDevice.Position = strongSelf.isUsingFrontCamera ? .front : .back
            guard let device = strongSelf.captureDevice(forPosition: cameraPosition) else {
                print("Failed to get capture device for camera position: \(cameraPosition)")
                return
            }
            do {
                strongSelf.captureSession.beginConfiguration()
                let currentInputs = strongSelf.captureSession.inputs
                for input in currentInputs {
                    strongSelf.captureSession.removeInput(input)
                }
                
                let input = try AVCaptureDeviceInput(device: device)
                guard strongSelf.captureSession.canAddInput(input) else {
                    print("Failed to add capture session input.")
                    return
                }
                strongSelf.captureSession.addInput(input)
                strongSelf.captureSession.commitConfiguration()
            } catch {
                print("Failed to create capture device input: \(error.localizedDescription)")
            }
        }
    }
    
    private func startSession() {
        weak var weakSelf = self
        sessionQueue.async {
            guard let strongSelf = weakSelf else {
                print("Self is nil!")
                return
            }
            strongSelf.captureSession.startRunning()
        }
    }
    
    private func stopSession(message: String) {
        weak var weakSelf = self
        sessionQueue.async {
            guard let strongSelf = weakSelf else {
                print("Self is nil!")
                return
            }
            strongSelf.captureSession.stopRunning()
            self.delegate?.onCancelScan(cancelError: message)
        }
    }
    
    private func setUpPreviewOverlayView() {
        cameraView.addSubview(previewOverlayView)
        NSLayoutConstraint.activate([
            previewOverlayView.topAnchor.constraint(equalTo: cameraView.topAnchor),
            previewOverlayView.bottomAnchor.constraint(equalTo: cameraView.bottomAnchor),
            previewOverlayView.leadingAnchor.constraint(equalTo: cameraView.leadingAnchor),
            previewOverlayView.trailingAnchor.constraint(equalTo: cameraView.trailingAnchor),
            
        ])
    }
    
    private func setUpAnnotationOverlayView() {
        cameraView.addSubview(annotationOverlayView)
        NSLayoutConstraint.activate([
            annotationOverlayView.topAnchor.constraint(equalTo: cameraView.topAnchor),
            annotationOverlayView.leadingAnchor.constraint(equalTo: cameraView.leadingAnchor),
            annotationOverlayView.trailingAnchor.constraint(equalTo: cameraView.trailingAnchor),
            annotationOverlayView.bottomAnchor.constraint(equalTo: cameraView.bottomAnchor),
        ])
    }
    
    private func captureDevice(forPosition position: AVCaptureDevice.Position) -> AVCaptureDevice? {
        if #available(iOS 10.0, *) {
            let discoverySession = AVCaptureDevice.DiscoverySession(
                deviceTypes: [.builtInWideAngleCamera],
                mediaType: .video,
                position: .unspecified
            )
            return discoverySession.devices.first { $0.position == position }
        }
        return nil
    }
    
    private func presentDetectorsAlertController() {
        let alertController = UIAlertController(
            title: Constant.alertControllerTitle,
            message: Constant.alertControllerMessage,
            preferredStyle: .alert
        )
        weak var weakSelf = self
        detectors.forEach { detectorType in
            let action = UIAlertAction(title: detectorType.rawValue, style: .default) {
                [unowned self] (action) in
                guard let value = action.title else { return }
                guard let detector = Detector(rawValue: value) else { return }
                guard let strongSelf = weakSelf else {
                    print("Self is nil!")
                    return
                }
                strongSelf.currentDetector = detector
                strongSelf.removeDetectionAnnotations()
            }
            if detectorType.rawValue == self.currentDetector.rawValue { action.isEnabled = false }
            alertController.addAction(action)
        }
        alertController.addAction(UIAlertAction(title: Constant.cancelActionTitleText, style: .cancel))
        present(alertController, animated: true)
    }
    
    private func removeDetectionAnnotations() {
        for annotationView in annotationOverlayView.subviews {
            annotationView.removeFromSuperview()
        }
    }
    
    private func updatePreviewOverlayViewWithLastFrame() {
        guard let lastFrame = lastFrame,
              let imageBuffer = CMSampleBufferGetImageBuffer(lastFrame)
        else {
            return
        }
        self.updatePreviewOverlayViewWithImageBuffer(imageBuffer)
        self.removeDetectionAnnotations()
    }
    
    private func updatePreviewOverlayViewWithImageBuffer(_ imageBuffer: CVImageBuffer?) {
        guard let imageBuffer = imageBuffer else {
            return
        }
        
        self.mainImageBuffer = imageBuffer
        
        let orientation: UIImage.Orientation = isUsingFrontCamera ? .leftMirrored : .left
        let image = UIUtilities.createUIImage(from: imageBuffer, orientation: orientation)
        previewOverlayView.image = image
        
        
    }
    
    private func convertedPoints(
        from points: [NSValue]?,
        width: CGFloat,
        height: CGFloat
    ) -> [NSValue]? {
        return points?.map {
            let cgPointValue = $0.cgPointValue
            let normalizedPoint = CGPoint(x: cgPointValue.x / width, y: cgPointValue.y / height)
            let cgPoint = previewLayer.layerPointConverted(fromCaptureDevicePoint: normalizedPoint)
            let value = NSValue(cgPoint: cgPoint)
            
            return value
        }
    }
    
    private func normalizedPoint(
        fromVisionPoint point: VisionPoint,
        width: CGFloat,
        height: CGFloat
    ) -> CGPoint {
        let cgPoint = CGPoint(x: point.x, y: point.y)
        var normalizedPoint = CGPoint(x: cgPoint.x / width, y: cgPoint.y / height)
        normalizedPoint = previewLayer.layerPointConverted(fromCaptureDevicePoint: normalizedPoint)
        return normalizedPoint
    }
    
    private func addContours(for face: Face, width: CGFloat, height: CGFloat) {
        if self.persentageLbl.text! != "Calibration in Progress..." {
            
            print("FACE ID : ====  \(face.trackingID)")
            if self.faceId == 0
            {
                self.faceId = face.trackingID
            }
            else if face.trackingID != self.faceId {
                print("Detect Multiple Faces")
                self.faceId = 0
                self.stopSession(message: "Unable to measure your vitals.\nTry to look at the camera the next time")
                self.timer?.invalidate()
                self.timer = nil
                self.counter = 0
                self.calibrationTime = 20
                self.delegate?.onScanFinished(raw_intensity: "", ppg_time: "", average_fps: self.fpsCount, finalData: "")
                UIApplication.shared.isIdleTimerDisabled = false
                self.dismiss(animated: true)
            }
        }
        
        // Cheeks
        if face != nil {
            
            self.cancel_timer?.invalidate()
            self.cancel_timer = nil
            self.cancel_counter = 0
            self.firstTime = 1
            
            //     top                    left                     bottum                   right
            if (face.frame.minX < 0 || face.frame.minY < 0 || face.frame.maxX > width || face.frame.maxY > height) {
                self.alertView.isHidden = false
                self.alertLbl.text! = "You are too close to the screen.\nPlease move a bit far."
                if self.persentageLbl.text! != "Calibration in Progress..." {
                    if self.first_fram_Time == 1 {
                        self.first_fram_Time = 0
                        if self.cancel_counter == 0 {
                            self.cancel_fram_timer = Timer.scheduledTimer(timeInterval: 1, target:self, selector:#selector(CancelFramTimer), userInfo: nil, repeats: true)
                        }
                    }
                }
            }
            else
            {
                self.cancel_fram_timer?.invalidate()
                self.cancel_fram_timer = nil
                self.cancel_fram_counter = 0
                self.first_fram_Time = 1
                
                self.isFaceDetected = true
                
                if self.cancel_counter > 0 {
                    self.cancel_timer?.invalidate()
                    self.cancel_timer = nil
                    self.cancel_counter = 0
                    self.alertView.isHidden = true
                    self.alertLbl.text! = ""
                }
                self.alertView.isHidden = true
                self.alertLbl.text! = ""
            }
            
            let leftEyelandmark = face.landmark(ofType: .leftEye)
            let cgPointEyeLeft = normalizedPoint(fromVisionPoint: leftEyelandmark!.position, width: width, height: height)
            let rightEyelandmark = face.landmark(ofType: .rightEye)
            let cgPointEyeRight = normalizedPoint(fromVisionPoint: rightEyelandmark!.position, width: width, height: height)
            
            
            /*if*/ let leftCheeklandmark = face.landmark(ofType: .leftCheek) //{
            let cgPointCheekLeft = normalizedPoint(fromVisionPoint: leftCheeklandmark!.position, width: width, height: height)
            let left_w = face.frame.width
            let left_h = abs((cgPointCheekLeft.y)-(cgPointEyeLeft.y))//(cgPointCheekRight.y)-(self.cgPointEyeRight.y)
            let left_hp = left_h*(0.5)
            let left_wp = left_w*(0.075)
            
            let left_x1 = Int((cgPointCheekLeft.x)-left_wp)
            let left_y1 = Int((cgPointCheekLeft.y)-left_hp)
            
            let left_x2 = Int((cgPointCheekLeft.x)+left_wp)
            let left_y2 = Int(cgPointCheekLeft.y)
            
            //            UIUtilities.addRectangle(CGRectMake((cgPointCheekLeft.x)-left_wp, (cgPointCheekLeft.y)-left_hp, 2*left_wp, left_hp), to: annotationOverlayView, color: UIColor.white)
            
            let leftColorAtPixel = self.getColor(image: previewOverlayView.image!, x1: left_x1, y1: left_y1, x2: left_x2, y2: left_y2)
            
            let left_r  = leftColorAtPixel["r"] ?? 0
            let left_g  = leftColorAtPixel["g"] ?? 0
            let left_b  = leftColorAtPixel["b"] ?? 0
            
            //            self.leftR = r
            //            self.leftG = g
            //            self.leftB = b
            
            //            print("letf \(Int(r)), \(Int(g)), \(Int(b))")
            //}
            
            /*if*/ let rightCheeklandmark = face.landmark(ofType: .rightCheek) //{
            let cgPointCheekRight = normalizedPoint(fromVisionPoint: rightCheeklandmark!.position, width: width, height: height)
            let right_w = face.frame.width
            let right_h = abs((cgPointCheekRight.y)-(cgPointEyeRight.y))//(cgPointCheekRight.y)-(self.cgPointEyeRight.y)
            let right_hp = right_h*(0.5)
            let right_wp = right_w*(0.075)
            
            let right_x1 = Int((cgPointCheekRight.x)-right_wp)
            let right_y1 = Int((cgPointCheekRight.y)-right_hp)
            
            let right_x2 = Int((cgPointCheekRight.x)+right_wp)
            let right_y2 = Int(cgPointCheekRight.y)
            
            //            UIUtilities.addRectangle(CGRectMake((cgPointCheekRight.x)-right_wp, (cgPointCheekRight.y)-right_hp, 2*right_wp, right_hp), to: annotationOverlayView, color: UIColor.white)
            
            //            if left_hp < left_wp || right_hp < right_wp {
            //                print("rotatin is on please stop ratation")
            //
            //            }
            
            
            let rightColorAtPixel = self.getColor(image: previewOverlayView.image!, x1: right_x1, y1: right_y1, x2: right_x2, y2: right_y2)
            
            let right_r  = rightColorAtPixel["r"] ?? 0
            let right_g  = rightColorAtPixel["g"] ?? 0
            let right_b  = rightColorAtPixel["b"] ?? 0
            
            //            self.rightR = r
            //            self.rightG = g
            //            self.rightB = b
            
            
            //            print("right \(Int(r)), \(Int(g)), \(Int(b))")
            
            //            NotificationCenter.default.post(name: Notification.Name("app"), object: nil)
            
            if self.counter < maxTime {
                
                let r = abs((left_r+right_r)/2)
                let g = abs((left_g+right_g)/2)
                let b = abs((left_b+right_b)/2)
                
                self.equipments.append(["r":r,
                                        "g":g,
                                        "b":b])
                
                //                print("both \(Int(r)), \(Int(g)), \(Int(b))")
                
                let EndTimeStamp = Int(NSDate().timeIntervalSince1970 * 1000)
                let elapsedTime = EndTimeStamp - (self.StartTimeStamp ?? 0)
                self.ppg_TimeArr.append(elapsedTime)
                
                print("self.calibrationTime: ======= \(self.calibrationTime)")
                if self.calibrationTime > 0 {
                    self.persentageLbl.text! = "Calibration in Progress..."
                    self.type = "Calibration"
                }
                else {
                    self.type = "Scan"
                    self.persentageLbl.text! = "\(lroundf(Float(percentageCompleted)))% Completed"
                    
                    UIApplication.shared.isIdleTimerDisabled = true
                    
                    if (self.currentTimeInMilliSeconds() - self.mfpsTime) > 1000 {
                        self.mfpsTime = self.currentTimeInMilliSeconds()
                        self.mfps = self.mfpsCounter
                        self.mfpsCounter = 0
                        //                        print("currentFpsdata:======\(self.mfps)")
                    }
                    else
                    {
                        self.mfpsCounter += 1
                        
                        //                        print("currentFps:======\(self.mfpsCounter)")
                    }
                    
                    self.delegate?.onFrame(type: self.type, percentage: Int(percentageCompleted), fps: self.mfps, isFaceDetected: self.isFaceDetected)
                    //                }
                    if self.counter == maxTime {
                        self.persentageLbl.text! = "Face Completed"
                        self.stopSession(message: "")
                        self.presentResult(average_fps: self.average_fps ?? 0)
                        
                        self.timer?.invalidate()
                        self.timer = nil
                        self.counter = 0
                        self.proceedLbl.text! = ""
                    }
                }
//                else {
//                    self.timer?.invalidate()
//                    self.timer = nil
//                    self.counter = 0
//                    self.proceedLbl.text! = ""
//                }
            }
        }
        
        self.fpsCount += 1
    }
    
    public func startFaceScan(minTime: Int, maxTime: Int, TextColor: String, BoxColor: String, BoxType: String) {
        
        self.maxTime = maxTime
        self.minTime = minTime
        self.TextColor = TextColor
        self.BoxColor = BoxColor
        self.BoxType = BoxType
        
//        print("startFaceScan:=== \(TextColor)")
    }
    
    public func StopScan(noCallback: Bool) {
        if (!noCallback || self.counter < self.minTime) {
            self.timer?.invalidate()
            self.timer = nil
            self.counter = 0
            self.calibrationTime = 20
            
            self.cancel_timer?.invalidate()
            self.cancel_timer = nil
            self.cancel_counter = 0
            
            self.cancel_fram_timer?.invalidate()
            self.cancel_fram_timer = nil
            self.cancel_fram_counter = 0
            
            self.delegate?.onScanFinished(raw_intensity: "", ppg_time: "", average_fps: self.fpsCount, finalData: "")
        }else {
            self.presentResult(average_fps: self.average_fps ?? 0)
        }
    }
    public func StopScan() {
        if (self.counter < self.minTime){
            self.timer?.invalidate()
            self.timer = nil
            self.counter = 0
            self.calibrationTime = 20
            
            self.cancel_timer?.invalidate()
            self.cancel_timer = nil
            self.cancel_counter = 0
            
            self.cancel_fram_timer?.invalidate()
            self.cancel_fram_timer = nil
            self.cancel_fram_counter = 0
            
            self.delegate?.onScanFinished(raw_intensity: "", ppg_time: "", average_fps: self.fpsCount, finalData: "")
        }else {
            self.presentResult(average_fps: self.average_fps ?? 0)
        }
    }
    
    func getColor(image: UIImage, x1: Int, y1: Int, x2: Int, y2: Int) -> [String:Double] {
        
        guard let pixelData = image.cgImage?.dataProvider?.data else { return ["r": 0, "g": 0, "b": 0] }
        let data: UnsafePointer<UInt8> = CFDataGetBytePtr(pixelData)
        
        var r: Double = 0
        var g: Double = 0
        var b: Double = 0
        var count = 0
        
        let pixelsWide = x2//Int(image.size.width * image.scale)
        let pixelsHigh = Int(image.size.height * image.scale)
        
        //  let widthRange = 0..<pixelsWide
        //  let heightRange = 0..<pixelsHigh
        
//        let widthThinner = Int(pixelsWide / pixelThinner) + 1
//        let heightThinner = Int(pixelsHigh / pixelThinner) + 1
//        let widthRange = stride(from: 0, to: pixelsWide, by: widthThinner)
//        let heightRange = stride(from: 0, to: pixelsHigh, by: heightThinner)
        
        for x in x1..<x2 {
            for y in y1..<y2 {
                let pixelInfo: Int = ((pixelsWide * y) + x) * 4
//                let color = "\(data[pixelInfo]).\(data[pixelInfo + 1]).\(data[pixelInfo + 2])"
                if pixelInfo < 700000 && pixelInfo > 0 {
                    r += Double(data[pixelInfo])
                    g += Double(data[pixelInfo + 1])
                    b += Double(data[pixelInfo + 2])
                    
                    count += 1
                }
                else {
                    r += 0.0
                    g += 0.0
                    b += 0.0
                    
                    count += 1
                }
            }
        }
        
        if r != 0 {
            r = r/Double(count)
        }
        else {
            r = 0
        }
        
        if g != 0 {
            g = g/Double(count)
        }
        else {
            g = 0
        }
        
        if b != 0 {
            b = b/Double(count)
        }
        else {
            b = 0
        }
        
        return ["r": r, "g": g, "b": b]
    }
    
    func presentResult(average_fps:Int){
        self.fpsCount = self.fpsCount/self.maxTime
        print("fps:========\(self.fpsCount)")
        print("ppg_time:========\(self.ppg_TimeArr)")
        var dict = ["raw_intensity" : self.equipments,
                    "ppg_time" : self.ppg_TimeArr,
                    "average_fps" : self.fpsCount] as [String : Any]
        
        
        let jsonData = try! JSONSerialization.data(withJSONObject: dict)
        var jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)
        print(jsonString)
        self.jsonString = jsonString as String?
        
        var dictPpg = self.ppg_TimeArr
        
        let jsonDataPpg = try! JSONSerialization.data(withJSONObject: dictPpg)
        var jsonStringPpg = NSString(data: jsonDataPpg, encoding: String.Encoding.utf8.rawValue)
        
        var dictRgb = self.equipments
        
        let jsonDataRgb = try! JSONSerialization.data(withJSONObject: dictRgb)
        var jsonStringRgb = NSString(data: jsonDataRgb, encoding: String.Encoding.utf8.rawValue)
        
        if self.counter > self.minTime {
            
            self.timer?.invalidate()
            self.timer = nil
            self.counter = 0
            self.proceedLbl.text! = ""
            
            self.delegate?.onScanFinished(raw_intensity: jsonStringRgb! as String, ppg_time: jsonStringPpg! as String, average_fps: self.fpsCount, finalData: jsonString! as String)
            self.stopSession(message: "")
        }
        
        print(self.average_fps, countB, countA)
        DispatchQueue.main.async {
            UIApplication.shared.isIdleTimerDisabled = false
            self.dismiss(animated: true)
        }
    }
    
    /// Resets any detector instances which use a conventional lifecycle paradigm. This method is
    /// expected to be invoked on the AVCaptureOutput queue - the same queue on which detection is
    /// run.
    private func resetManagedLifecycleDetectors(activeDetector: Detector) {
        if activeDetector == self.lastDetector {
            // Same row as before, no need to reset any detectors.
            return
        }
       
        self.lastDetector = activeDetector
    }
    
    private func rotate(_ view: UIView, orientation: UIImage.Orientation) {
        var degree: CGFloat = 0.0
        switch orientation {
        case .up, .upMirrored:
            degree = 90.0
        case .rightMirrored, .left:
            degree = 180.0
        case .down, .downMirrored:
            degree = 270.0
        case .leftMirrored, .right:
            degree = 0.0
        }
        view.transform = CGAffineTransform.init(rotationAngle: degree * 3.141592654 / 180)
    }
}

// MARK: AVCaptureVideoDataOutputSampleBufferDelegate

extension CameraViewController: AVCaptureVideoDataOutputSampleBufferDelegate {
    
    public func captureOutput(
        _ output: AVCaptureOutput,
        didOutput sampleBuffer: CMSampleBuffer,
        from connection: AVCaptureConnection
    ) {
        guard let imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer) else {
            print("Failed to get image buffer from sample buffer.")
            return
        }
        // Evaluate `self.currentDetector` once to ensure consistency throughout this method since it
        // can be concurrently modified from the main thread.
        let activeDetector = self.currentDetector
        resetManagedLifecycleDetectors(activeDetector: activeDetector)
        
        lastFrame = sampleBuffer
        let visionImage = VisionImage(buffer: sampleBuffer)
        let orientation = UIUtilities.imageOrientation(
            fromDevicePosition: isUsingFrontCamera ? .front : .back
        )
        visionImage.orientation = orientation
        
        guard let inputImage = MLImage(sampleBuffer: sampleBuffer) else {
            print("Failed to create MLImage from sample buffer.")
            return
        }
        inputImage.orientation = orientation
        
        let imageWidth = CGFloat(CVPixelBufferGetWidth(imageBuffer))
        let imageHeight = CGFloat(CVPixelBufferGetHeight(imageBuffer))
        var shouldEnableClassification = false
        var shouldEnableMultipleObjects = false
        
        switch activeDetector {
            
        case .onDeviceFace:
            detectFacesOnDevice(in: visionImage, width: imageWidth, height: imageHeight)
        }
    }
}

// MARK: - Constants

public enum Detector: String {
    case onDeviceFace = "Face Detection"
    
}

private enum Constant {
    static let alertControllerTitle = "Vision Detectors"
    static let alertControllerMessage = "Select a detector"
    static let cancelActionTitleText = "Cancel"
    static let videoDataOutputQueueLabel = "com.google.mlkit.visiondetector.VideoDataOutputQueue"
    static let sessionQueueLabel = "com.google.mlkit.visiondetector.SessionQueue"
    static let noResultsMessage = "No Results"
    static let localModelFile = (name: "bird", type: "tflite")
    static let labelConfidenceThreshold = 0.75
    static let smallDotRadius: CGFloat = 4.0
    static let lineWidth: CGFloat = 3.0
    static let originalScale: CGFloat = 1.0
    static let padding: CGFloat = 10.0
    static let resultsLabelHeight: CGFloat = 200.0
    static let resultsLabelLines = 5
    static let imageLabelResultFrameX = 0.4
    static let imageLabelResultFrameY = 0.1
    static let imageLabelResultFrameWidth = 0.5
    static let imageLabelResultFrameHeight = 0.8
    static let segmentationMaskAlpha: CGFloat = 0.5
}
